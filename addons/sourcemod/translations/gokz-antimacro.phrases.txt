"Phrases"
{
    // General ////////////////////////////////////////////////////////////
    "Perfs"
    {
        "en"        "Perfs"
    }
    "Average"
    {
        "en"        "Average"
    }
    "Pattern"
    {
        "en"        "Pattern"
    }
    
    
    // Chat Messages ////////////////////////////////////////////////////////////
    "Antimacro Warning"
    {
        "en"        "{darkred}WARNING! The use of software, macros, hyperscroll, or any other external assistance to aid character movement is strictly not allowed and may result in an automatic ban."
    }
    "Not Enough Bhops"
    {
    	// Bill has not bhopped enough for a bhop check.
		"#format"	"{1:N}"
        "en"        "{lime}{1} {grey}has not bhopped enough for a bhop check."
    }
    "Not Enough Bhops (Self)"
    {
        "en"        "{grey}You have not bhopped enough for a bhop check."
    }
    
    
    // Console Messages ////////////////////////////////////////////////////////////
    "Not Enough Bhops (Console)"
    {
    	// Bill has not bhopped enough for a bhop check. Skipping...
		"#format"	"{1:N}"
        "en"        "{1} has not bhopped enough for a bhop check. Skipping..."
    }
}